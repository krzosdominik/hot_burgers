## Getting Started

### Project info
Hot Burgers website based on a XD mock-up. Technologies: Sass, Gulp and Bootstrap

### Prerequisites

* Node js
```sh
https://nodejs.org/dist/v12.15.0/node-v12.15.0-x64.msi
```
* npm
```sh
npm install npm@latest -g
```

### Installation

* Clone the repo
```sh
git clone https://krzosdominik@bitbucket.org/krzosdominik/hot_burgers.git
```
* Install NPM packages
```sh
npm install
```
* Run Gulp
```sh
gulp
```